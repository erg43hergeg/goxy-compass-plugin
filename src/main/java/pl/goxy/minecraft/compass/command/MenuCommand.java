package pl.goxy.minecraft.compass.command;

import com.github.stefvanschie.inventoryframework.gui.type.util.Gui;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import pl.goxy.minecraft.compass.GuiService;
import pl.goxy.minecraft.compass.MenuService;
import pl.goxy.minecraft.compass.configuration.MenuConfiguration;

public class MenuCommand
        implements CommandExecutor
{
    private final MenuService menuService;
    private final GuiService guiService;

    public MenuCommand(MenuService menuService, GuiService guiService)
    {
        this.menuService = menuService;
        this.guiService = guiService;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label,
            @NotNull String[] args)
    {
        if (!(sender instanceof Player))
        {
            return true;
        }
        String name = args.length > 0 ? args[0] : "default";
        if (!sender.hasPermission("compass.command.menu." + name))
        {
            return true;
        }
        MenuConfiguration configuration = this.menuService.get(name);
        if (configuration == null)
        {
            return true;
        }
        Gui menu = this.guiService.getMenu(configuration, sender);
        menu.show((Player) sender);
        return true;
    }
}
