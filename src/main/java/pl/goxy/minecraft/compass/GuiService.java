package pl.goxy.minecraft.compass;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.*;
import com.github.stefvanschie.inventoryframework.gui.type.util.Gui;
import com.github.stefvanschie.inventoryframework.gui.type.util.MergedGui;
import com.github.stefvanschie.inventoryframework.gui.type.util.NamedGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permissible;
import pl.goxy.minecraft.compass.action.ItemAction;
import pl.goxy.minecraft.compass.configuration.ItemConfiguration;
import pl.goxy.minecraft.compass.configuration.ItemPosition;
import pl.goxy.minecraft.compass.configuration.MenuConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GuiService
{
    private static final Map<String, Function<String, NamedGui>> MAPPINGS = new HashMap<>();

    static
    {
        MAPPINGS.put("anvil", AnvilGui::new);
        MAPPINGS.put("dispenser", DispenserGui::new);
        MAPPINGS.put("furnace", FurnaceGui::new);
        MAPPINGS.put("hopper", HopperGui::new);
    }

    public Gui getMenu(MenuConfiguration configuration, Permissible permissible)
    {
        NamedGui gui = resolveGuiFromConfig(configuration);
        gui.setOnGlobalClick(event -> event.setCancelled(true));

        if (configuration.getBackground() != null)
        {
            OutlinePane background = new OutlinePane(0, 0, 9, configuration.getHeight(), Pane.Priority.LOWEST);
            background.addItem(this.getItem(configuration.getBackground(), permissible));
            background.setRepeat(true);

            if (gui instanceof MergedGui)
            {
                MergedGui mergedGui = (MergedGui) gui;
                mergedGui.addPane(background);
            }
        }

        StaticPane navigation = new StaticPane(0, 0, 9, configuration.getHeight());
        configuration.getItems().forEach(itemConfiguration ->
        {
            String showPermission = itemConfiguration.getShowPermission();
            if (showPermission != null && !permissible.hasPermission(showPermission))
            {
                return;
            }
            ItemPosition position = itemConfiguration.getPosition();
            if (position != null)
            {
                navigation.addItem(this.getItem(itemConfiguration, permissible), position.getX(), position.getY());
            }
        });

        if (gui instanceof MergedGui)
        {
            MergedGui mergedGui = (MergedGui) gui;
            mergedGui.addPane(navigation);
        }

        return gui;
    }

    public GuiItem getItem(ItemConfiguration configuration, Permissible permissible)
    {
        ItemAction action = configuration.getAction();
        Function<String, String> replacer = action != null ? action.getReplacer() : Function.identity();
        ItemStack item = this.getItemStack(configuration, replacer);
        if (action != null)
        {
            return new GuiItem(item, event ->
            {
                String clickPermission = configuration.getClickPermission();
                if (clickPermission != null && !permissible.hasPermission(clickPermission))
                {
                    return;
                }
                if (event.getWhoClicked() instanceof Player)
                {
                    action.accept((Player) event.getWhoClicked());
                }
            });
        }
        return new GuiItem(item);
    }

    public ItemStack getItemStack(ItemConfiguration configuration, Function<String, String> replacer)
    {
        ItemStack item = new ItemStack(configuration.getType());
        ItemMeta meta = item.getItemMeta();
        if (meta == null)
        {
            return item;
        }
        if (configuration.getName() != null)
        {
            String name = translateAlternateColorCodes(replacer.apply(configuration.getName()));
            meta.setDisplayName(name);
        }
        else
        {
            meta.setDisplayName(ChatColor.RESET.toString());
        }
        if (configuration.getLore() != null)
        {
            List<String> lore = configuration.getLore()
                    .stream()
                    .map(replacer)
                    .map(GuiService::translateAlternateColorCodes)
                    .collect(Collectors.toList());
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        return item;
    }

    private static NamedGui resolveGuiFromConfig(MenuConfiguration configuration)
    {
        String title = translateAlternateColorCodes(configuration.getTitle());

        String type = configuration.getType();
        String lowered = type.toLowerCase();

        if (lowered.equals("chest"))
        {
            int height = configuration.getHeight();
            return new ChestGui(height, title);
        }

        Function<String, NamedGui> function = MAPPINGS.get(lowered);

        return function.apply(title);
    }

    private static String translateAlternateColorCodes(String text)
    {
        return ChatColor.translateAlternateColorCodes('&', text);
    }
}
