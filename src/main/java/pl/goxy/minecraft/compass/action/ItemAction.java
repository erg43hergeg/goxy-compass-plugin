package pl.goxy.minecraft.compass.action;

import org.bukkit.entity.Player;

import java.util.function.Consumer;
import java.util.function.Function;

public interface ItemAction
        extends Consumer<Player>
{
    Function<String, String> getReplacer();
}
