package pl.goxy.minecraft.compass.configuration;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class InventoryConfiguration
{
    private boolean clear;
    private List<ItemConfiguration> items = new ArrayList<>();

    public boolean isClear()
    {
        return clear;
    }

    public void setClear(boolean clear)
    {
        this.clear = clear;
    }

    @NotNull
    public List<ItemConfiguration> getItems()
    {
        return items;
    }

    public void setItems(@NotNull List<ItemConfiguration> items)
    {
        this.items = items;
    }
}
