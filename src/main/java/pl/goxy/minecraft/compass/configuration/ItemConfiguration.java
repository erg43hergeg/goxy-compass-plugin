package pl.goxy.minecraft.compass.configuration;

import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.goxy.minecraft.compass.action.ItemAction;

import java.util.List;

public class ItemConfiguration
{
    private final Material type;
    private ItemPosition position;
    private String name;
    private List<String> lore;
    private ItemAction action;

    private String showPermission;
    private String clickPermission;

    public ItemConfiguration(Material type)
    {
        this.type = type;
    }

    @NotNull
    public Material getType()
    {
        return type;
    }

    @Nullable
    public ItemPosition getPosition()
    {
        return position;
    }

    public void setPosition(@Nullable ItemPosition position)
    {
        this.position = position;
    }

    @Nullable
    public String getName()
    {
        return name;
    }

    public void setName(@Nullable String name)
    {
        this.name = name;
    }

    @Nullable
    public List<String> getLore()
    {
        return lore;
    }

    public void setLore(@Nullable List<String> lore)
    {
        this.lore = lore;
    }

    @Nullable
    public ItemAction getAction()
    {
        return action;
    }

    public void setAction(@Nullable ItemAction action)
    {
        this.action = action;
    }

    @Nullable
    public String getShowPermission()
    {
        return showPermission;
    }

    public void setShowPermission(@Nullable String showPermission)
    {
        this.showPermission = showPermission;
    }

    @Nullable
    public String getClickPermission()
    {
        return clickPermission;
    }

    public void setClickPermission(@Nullable String clickPermission)
    {
        this.clickPermission = clickPermission;
    }
}
