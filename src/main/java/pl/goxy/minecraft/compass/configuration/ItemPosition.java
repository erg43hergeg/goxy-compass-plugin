package pl.goxy.minecraft.compass.configuration;

public class ItemPosition
{
    private final int slot;

    public ItemPosition(int slot)
    {
        this.slot = slot;
    }

    public ItemPosition(int x, int y)
    {
        this(9 * y + x);
    }

    public int getSlot()
    {
        return slot;
    }

    public int getX()
    {
        return this.getSlot() - (this.getY() * 9);
    }

    public int getY()
    {
        return this.getSlot() / 9;
    }
}
